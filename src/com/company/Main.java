/**
 * Created by Henok Million on 5/16/18.
 * ID: ATR/1810/08
 * SE - section 2 (R)
 */
package com.company;
import java.util.HashMap;
import java.util.Random;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

public class Main {
    public static void main(String[] args) {
        Oracle oracle = new Oracle();
        oracle.start();
    }
}
class Registration {
    HashMap<String, String> users = new HashMap<String, String>();

    public HashMap<String, String> getUsers() {
        return users;
    }
    int getUserSize() {
        return users.size();
    }
    void addUser(String name) {
        this.users.put("name", name);
    }
}

class Oracle extends Thread {
    Registration registration = new Registration();
    AtomicBoolean running = new AtomicBoolean(true);

    @Override
    public void run() {
        try {
            System.out.println(this.testAddUser());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    String testAddUser() {
        int usersSize = registration.getUserSize();
        RandomGenerator generator = new RandomGenerator();
        registration.addUser(generator.getFullName());
        if (usersSize < registration.getUserSize()) {
            return "Success";
        }
        return "Fail";
    }
}

class RandomGenerator {
    String names[] = {"Habtamu", "Kassahun", "Haile", "Bishaw", "Merid", "Mesfin",
            "Lemma", "Biruk", "Robel", "Hizkel"};

    String getFullName() {
        StringBuilder fullname = new StringBuilder();
        for (int i = 0; i < 2; i++) {
            int count = Math.abs(new Random().nextInt());
            if (count > 10) {
                count = count % 10;
            }
            fullname.append(names[count]);
            if (i < 1) fullname.append(" ");
        }
        return fullname.toString();
    }
}
